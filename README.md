# Configuring Docker to Use a Proxy on Linux

This guide provides step-by-step instructions on how to configure Docker to bypass a proxy server when operating on a Linux PC. By setting up these configurations, Docker can pull images and build containers even when behind a corporate proxy or a similar network setup.

## Description

Docker requires internet access to pull container images from registries unless they are already cached locally. If your Linux PC is behind a proxy, you need to configure Docker to use the proxy settings to ensure it can reach external services.

## Prerequisites

Ensure you have the following prerequisites covered:

- Docker installed on your Linux PC.
- Administrative access to modify Docker's configuration.
- The IP address and port of your proxy server.

## Step by Step Guide

### 1. Configure the Docker Client

Create a `.docker` directory and a `config.json` file in your home directory:

```bash
mkdir -p ~/.docker
nano ~/.docker/config.json
```
#### 1.1 Add the following proxy configuration, replacing `yourProxyIP:yourProxyPort` with your actual proxy details:

```json
{
  "proxies":
  {
    "default":
    {
      "httpProxy": "http://yourProxyIP:yourProxyPort",
      "httpsProxy": "http://yourProxyIP:yourProxyPort",
      "noProxy": "localhost,127.0.0.1"
    }
  }
}
```
Save and exit the editor.

### 2. Configure the Docker Daemon

Create or edit a http-proxy.conf file for the Docker service:

```sh
sudo mkdir -p /etc/systemd/system/docker.service.d
sudo nano /etc/systemd/system/docker.service.d/http-proxy.conf
```

#### 2.1 Add the following content, again replacing `yourProxyIP:yourProxyPort` with your proxy details:

```sh
[Service]
Environment="HTTP_PROXY=http://yourProxyIP:yourProxyPort"
Environment="HTTPS_PROXY=http://yourProxyIP:yourProxyPort"
Environment="NO_PROXY=localhost,127.0.0.1"
```

Save and close the file.

### 3. Apply the Configuration

Reload the systemd manager configuration and restart Docker to apply the new proxy settings:

```sh
sudo systemctl daemon-reload
sudo systemctl restart docker
```

### 4. Testing the Configuration

Test your configuration by pulling the hello-world image:

```sh
docker pull hello-world
```

If the image is pulled successfully, your Docker is now configured to use the proxy.

## Troubleshooting
If you encounter any issues, verify the following:

- Your proxy server is operational.
- You've used the correct IP address and port.
- You do not need to authenticate with the proxy. If authentication is required, you will need to include your username and password in the proxy URL.

## Conclusion

With the above steps, you should have Docker configured to use a proxy server on your Linux PC. This allows you to pull Docker images and manage containers behind a proxy.